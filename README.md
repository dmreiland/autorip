# autorip

A little bit of shell and ruby goodness to automate ripping audio CDs, DVDs, and Bluray discs via udev.

The benefit of using udev: this runs headless.

## Required Packages

* at
* dvdbackup
* eyeD3
* cd-discid
* mp3enc
* lame
* abcde
* wodim (cdrecord)
* imdb (gem)
* aws-sdk-v1 (gem)
* twilio-ruby (gem)

## Required Infrastructure

* An Amazon Web Services account
* A domain name (for setting up SES email notifications)
* A Twilio account (for SMS notifications)

## Installation

1. Modify 90-rom-autorip.rules with the name of your CD/DVD/BLURAY-ROM (typically sr0 if you only have one in your system)

1. Place the modified 90-rom-autorip.rules in /etc/udev/rules.d

1. Place the autorip and aurorip.rb scripts in /usr/local/bin; be sure to chmod a+x on them

1. Place the .abcde.conf file in your /root directory; modify config to suit your environment

1. Sign up for an Amazon Web Services account. Create an IAM user with limited permissions (Simple Email Service only)

1. In the AWS Console, create an IAM user with limited permissions (Simple Email Service only): note the credentials provided after creation

1. Register a verified sender email address via the AWS Console under the SES service

1. Update the autorip wrapper script with the credentials you noted above

1. Update the autorip.rb script with your email address(es) for notifications: $from_address and $recipients

1. Install libdvdcss

     /usr/share/doc/libdvdread4/install-css.sh

1. profit

## Reference Documentation

http://askubuntu.com/questions/359855/how-to-detect-insertion-of-dvd-disc


## Mythbuntu Configuration

This configuration closely follows that provided by [Pat Hartl](http://pathartl.me/blog/2013/12/01/the-ultimate-automated-ripping-machine). A massive thanks for Pat for providing excellent documentation and the initial inspiration for this endeavor. A few changes were made for Ubuntu 14.04.

## Add a Few Repositories

      add-apt-repository ppa:stebbins/handbrake-releases
      add-apt-repository ppa:brightbox/ruby-ng

### Install Required Packages

* eyeD3
* at
* cd-discid
* lame
* mp3enc
* abcde
* dvdbackup
* handbrake-gtk
* handbrake-cli
* build-essential
* libc6-dev
* libssl-dev
* libexpat1-dev
* libavcodec-dev
* libgl1-mesa-dev
* libqt4-dev
* vim-nox
* git-core
* p7zip-full
* xbmc-eventclients-xbmc-send
* regionset
* libdiscid0
* ruby2.1
* ruby2.1-dev

      apt-get install -y eyeD3 at cd-discid lame wodim dvdbackup handbrake-gtk \
                         handbrake-cli build-essential libc6-dev libssl-dev \
                         libexpat1-dev libavcodec-dev libgl1-mesa-dev mp3enc \
                         libqt4-dev vim-nox git-core p7zip-full abcde \
                         xbmc-eventclients-xbmc-send regionset libdiscid0 ruby2.1 ruby2.1-dev

* Install dvdcss kit
      
      /usr/share/doc/libdvdread4/install-css.sh

With the above in place (scripts and Mythbuntu config), you should be all set.

