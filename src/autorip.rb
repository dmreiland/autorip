#!/usr/bin/env ruby
require 'syslog/logger'
require 'fileutils'
require 'imdb'
require 'aws-sdk-v1'
require 'twilio-ruby'

AWS.config
$source_drive = '/dev/sr0'
$output_dir = '/var/lib/mythtv/videos'
$temp_dir = '/tmp'
$encode_profile = 'High Profile' # see: https://trac.handbrake.fr/wiki/BuiltInPresets
$from_address = 'no-reply@email.com'
$recipients = ['recipient1@mail.com', 'recipient2@mail.com']
$sms_sender = '+16125551212'
$sms_recipient = '+16125551212'
ENV['PATH'] = '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

$log = Syslog::Logger.new 'autorip'

# debug
#$log.info "PATH => #{ENV['PATH']}"
#$log.info "ID_CDROM_MEDIA => #{ENV['ID_CDROM_MEDIA']}"

def identify_media
  $log.info 'identifying media'
  media_type = `cdrecord -v dev=#{$source_drive} -toc | grep 'Current:' | awk {'print $3'} | tr -d '()'`.chomp
  $log.info "detected #{media_type}"
  media_type
end

def send_sms(recipient, msg)
  $log.info "sending sms notification to #{recipient}"
  Twilio::REST::Client.new(ENV['TWILIO_SID'], ENV['TWILIO_TOKEN']).messages.create(
    from:$sms_sender,
    to:recipient,
    body:msg
  )
end

def send_movie_notification(movie)
  $log.info "sending email notification to #{$recipients}"
  AWS::SimpleEmailService.new.send_email(
    :subject => "autorip complete for '#{movie.title}'",
    :from => $from_address,
    :to => $recipients,
    :body_html => "
      <h1 style=\"font-size: 250%;font-family: HelveticaNeue-Light, 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: normal;\">ohai!</h1>
      <h2 style=\"font-size: 175%;font-family: HelveticaNeue-Light, 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: normal;\"><a href=\"#{movie.url}\">#{movie.title}</a> is ready for viewing!</h2>
      <img height='444px' width='300px' src=\"#{movie.poster}\" />"
  )
end

def send_fail_notification(title)
  $log.info "sending email notification to #{$recipients}"
  AWS::SimpleEmailService.new.send_email(
    :subject => "autorip failed for '#{title}'",
    :from => $from_address,
    :to => $recipients,
    :body_html => "
      <h1 style=\"font-size: 250%;font-family: HelveticaNeue-Light, 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: normal;\">shit.</h1>
      <h2 style=\"font-size: 175%;font-family: HelveticaNeue-Light, 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: normal;\">autorip failed for \"#{title}\" due to an encoding failure.</h2>
      <p style=\"font-size: 125%;font-family: HelveticaNeue-Light, 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: normal;\">Don't worry, we kept all of the files around so you can divine the agent of cause.</p>
      <p style=\"font-size: 125%;font-family: HelveticaNeue-Light, 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: normal;\">Good luck with that.</p>"
  )
end

def send_music_notification
  $log.info "sending email notification to #{$recipients}"
  AWS::SimpleEmailService.new.send_email(
    :subject => "autorip complete for your latest album",
    :from => $from_address,
    :to => $recipients,
    :body_html => "
      <h1 style=\"font-size: 250%;font-family: HelveticaNeue-Light, 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: normal;\">ohai!</h1>
      <h2 style=\"font-size: 175%;font-family: HelveticaNeue-Light, 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: normal;\">The latest album you fed me is now ready for listening.</h2>
      <img src='http://unixsherpa-public.s3.amazonaws.com/assets/hungry-1.jpg'>
      <br>
      <p>What say you feed me another?</p>"
  )
end

def eject_drive
  $log.info "ejecting drive"
  `eject #{$source_drive}`
end

def cleanup(item)
  $log.info "cleaning up #{item}"
  FileUtils.rm_rf item
end

def rip_audio
  $log.info "starting audio rip"
  system "cd #{$temp_dir}; abcde -c /root/.abcde.conf"
  $log.info "audio rip complete"
end

def check_for_running_encode
  begin
    running_job_id = `ps aux | grep H[a]ndBrakeCLI | awk {'print $2'}`.chomp
    raise if running_job_id.length > 0
  rescue
    $log.info "previous encode job [#{running_job_id}] running, sleeping for 30 seconds"
    sleep 30
    retry
  end
end

def encode_movie(extension, title, disc_name)
  begin
    $log.info "starting encode for #{title}"
    system "HandBrakeCLI --main-feature -i \"#{$temp_dir}/#{disc_name}\" -o \"#{$output_dir}/#{title}.#{extension}\" --preset=\"#{$encode_profile}\" 2>&1 | logger -t handbrake"
    unless $? == 0
      raise
    end
    $log.info "encode for #{title} complete"
  rescue
    $log.info 'encodeFailure encountered; sending notification and terminating execution'
    send_fail_notification(title)
    send_sms $sms_recipient, "Shit, I couldn't encode #{movie.title}. Best you check into that."
    abort("encode failure")
  end
end

def get_disc_name
  `blkid -o value -s LABEL #{$source_drive}`.chomp
end

def get_movie_info(disc_name)
  begin
    $log.info "searching IMDB for #{disc_name}"
    raise unless movie = Imdb::Search.new(disc_name).movies[0]
    $log.info "found probable match: #{movie.title}"
  rescue
    $log.info "unable to find a match for #{disc_name}, returning a generic object"
    Struct.new('Movie', :title, :url, :poster)
    movie = Struct::Movie.new(disc_name, '#', 'https://s3.amazonaws.com/unixsherpa-public/Grumpy-Cat.jpg')
  end
  movie
end

def rip_title(title, disc_name)
  $log.info "starting rip for #{title}"
  system "mkdir \"#{$temp_dir}/#{disc_name}\""
  system "makemkvcon mkv --cache=16 --noscan -r --minlength=600 disc:0 all \"#{$temp_dir}/#{disc_name}\" 2>&1 | logger -t makemkv"
  $log.info "rip for #{title} completed with exit code #{$?.exitstatus}"
end

# main
if ENV['ID_CDROM_MEDIA']
  $log.info "starting autorip"
  media = identify_media

  case media
  when 'CD-ROM', 'CD-R'
    rip_audio
    send_music_notification
    eject_drive
  when 'DVD-ROM', 'DVD-R', 'BLURAY-ROM'
    disc_name = get_disc_name
    movie = get_movie_info disc_name
    rip_title movie.title, disc_name
    eject_drive
    send_sms $sms_recipient, "Yo, I just finished ripping #{movie.title}. Feed me."
    check_for_running_encode
    encode_movie 'mkv', movie.title, disc_name
    send_sms $sms_recipient, "w00t! Finished encoding #{movie.title}."
    cleanup "#{$temp_dir}/#{disc_name}"
    send_movie_notification movie
  else
    $log.info "action not defined for media type #{media}"
    send_sms $sms_recipient, "Aw snap, I couldn't figure out what to do with media type #{media}. Giving up."
    eject_drive
  end
end
